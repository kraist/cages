$(document).ready(function(){
	$('.title__button').click(function(){
		$('.panel').toggleClass('panel__active');
		
	});
	$('.panel__members_file').click(function(){
		$('.popup').css('display', 'flex');
		$('.fon').show();
	});
	$('.popup__close').click(function(){
		$(this).parent().hide();
		$('.fon').hide();
	});
	$('.level__block_addAdmin').click(function(){
		$('.popup__setting').css('display', 'flex');
	});
	$('.popup__setting_buttons-close').click(function(){
		$(this).parent().parent().hide();
	});
	$('.panel__members_arrow').click(function(){
		$('.panel__members_list').toggleClass('panel__members_list-active');
		$(this).toggleClass('panel__members_arrow-active');
	});
	$('.popup__setting_box').click(function(){
		$(this).toggleClass('popup__setting_active');
	});
	$(".aphorisms__click").click(function () {
		$('.aphorisms__tab5').hide();
		var index = $(this).index();
		index++;
		$('.open'+index).css('display','flex');
		$(".aphorisms__click").removeClass("aphorisms__block_numbers-number--active");
		$(this).addClass("aphorisms__block_numbers-number--active");	
	});
	$(".companies__click").click(function () {
		$('.companies__tab5').hide();
		var index = $(this).index();
		index++;
		$('.opendiv'+index).css('display','flex');
		$(".companies__click").removeClass("aphorisms__block_numbers-number--active");
		$(this).addClass("aphorisms__block_numbers-number--active");	
	});
	$(".formodels__click").click(function () {
		$('.formodels__tab5').hide();
		var index = $(this).index();
		index++;
		$('.openbox'+index).css('display','flex');
		$(".formodels__click").removeClass("aphorisms__block_numbers-number--active");
		$(this).addClass("aphorisms__block_numbers-number--active");	
	});
	$(".blog__click").click(function () {
		$('.blog__tab5').hide();
		var index = $(this).index();
		index++;
		$('.blogbox'+index).css('display','flex');
		$(".blog__click").removeClass("aphorisms__block_numbers-number--active");
		$(this).addClass("aphorisms__block_numbers-number--active");	
	});
	$(".panel__settings_list0").click(function () {
		$('.panei__tab3').css('opacity','0');
		$('.panei__tab3').css('z-index','0');
		$('.panei__tab3').removeClass('panelopen--active');
		$('.nuttt').removeClass('nuttt__active');
		$('.admin').removeClass('admin__active');
		$('.nut').removeClass('nut__active');
		$('.panel__members_list-box').removeClass('panel__members_list-box--active');
		var index = $(this).index();
		index++;
		$('.panelopen'+index).css('opacity','1');
		$('.panelopen'+index).css('z-index','1000');
		$('.panelopen'+index).addClass('panelopen--active');
		$(".panel__settings_list0").removeClass("panel__settings_list0--active");
		$(this).addClass("panel__settings_list0--active");	
	});
	
	$('.nut').click(function(){
		$('.nuttt').addClass('nuttt__active');
		$('.admin').removeClass('admin__active');
		$('.panei__tab3').css('opacity','0');
		$('.panei__tab3').css('z-index','0');
		$('.panei__tab3').removeClass('panelopen--active');
		$('.panel__settings_list0').removeClass('panel__settings_list0--active');
		$('.panel__members_list-box').removeClass('panel__members_list-box--active');
		$(this).addClass('nut__active');
	});
	
	$('.panel__members_list-box').click(function(){
		$('.admin').addClass('admin__active');
		$('.nuttt').removeClass('nuttt__active');
		$('.panei__tab3').css('opacity','0');
		$('.panei__tab3').css('z-index','0');
		$('.panei__tab3').removeClass('panelopen--active');
		$('.nut').removeClass('nut__active');
		$('.panel__settings_list0').removeClass('panel__settings_list0--active');
		$(this).addClass('panel__members_list-box--active');
	});
	
	
	$('.allmodels__click').click(function(){
		$('.allmodels__click').removeClass('allmodels__click_active');
		$(this).addClass('allmodels__click_active');
	});
	$('.aphorisms__block_setting-button1').click(function(){
		$(this).toggleClass('aphorisms__block_setting-button--active');
	});
	$('.aphorisms__block_setting-button3--open1').click(function(){
		$('.aphorisms__block_text-input--open1').attr('value','');
	});
	$('.aphorisms__block_setting-button3--open2').click(function(){
		$('.aphorisms__block_text-input--open2').attr('value','');
	});
	$('.aphorisms__block_setting-button3--open3').click(function(){
		$('.aphorisms__block_text-input--open3').attr('value','');
	});
	$('.aphorisms__block_setting-button3--open4').click(function(){
		$('.aphorisms__block_text-input--open4').attr('value','');
	});
	
	$('.aphorisms__block_setting-button3--opendiv1').click(function(){
		$('.companies__question1').attr('value','');
		$('.companies__answer1').attr('value','');
	});
	$('.aphorisms__block_setting-button3--opendiv2').click(function(){
		$('.companies__question2').attr('value','');
		$('.companies__answer2').attr('value','');
	});
	$('.aphorisms__block_setting-button3--opendiv3').click(function(){
		$('.companies__question3').attr('value','');
		$('.companies__answer3').attr('value','');
	});
	$('.aphorisms__block_setting-button3--opendiv4').click(function(){
		$('.companies__question4').attr('value','');
		$('.companies__answer4').attr('value','');
	});
	
	$('.aphorisms__block_setting-button3--openbox1').click(function(){
		$('.companies__question11').attr('value','');
		$('.companies__answer11').attr('value','');
	});
	$('.aphorisms__block_setting-button3--openbox2').click(function(){
		$('.companies__question22').attr('value','');
		$('.companies__answer22').attr('value','');
	});
	$('.aphorisms__block_setting-button3--openbox3').click(function(){
		$('.companies__question33').attr('value','');
		$('.companies__answer33').attr('value','');
	});
	$('.aphorisms__block_setting-button3--openbox4').click(function(){
		$('.companies__question44').attr('value','');
		$('.companies__answer44').attr('value','');
	});
	
	$('.addnewfile_file1').click(function(){
		$(this).addClass('addnewfile_file-active');
		$('.addnewfile_file_text-word').text('100%');
	});
	$('body').on('click', '.open__form_eye', function(){
		if ($('.open__form_input-password').attr('type') == 'password'){
			$('.open__form_input-password').attr('type', 'text');
		} else {
			$('.open__form_input-password').attr('type', 'password');
		}
		return false;
	});
	$('.allmodels__slider_slide').slick({
		dots: false,
		arrows: true,
		slidesToShow: 1,
        slidesToScroll: 1,
		infinite: true
	});
	$('.allmodels__select1').click(function(){
		$('.allmodels__dropdown1').toggleClass('jq-selectbox__dropdown-active');
	});
	$('.allmodels__dropdown1 li').click(function(){
		$('.allmodels__dropdown1').toggleClass('jq-selectbox__dropdown-active');
		$('.allmodels__select-text1').text($(this).text());
	});
	$('.allmodels__select2').click(function(){
		$('.allmodels__dropdown2').toggleClass('jq-selectbox__dropdown-active');
	});
	$('.allmodels__dropdown2 li').click(function(){
		$('.allmodels__dropdown2').toggleClass('jq-selectbox__dropdown-active');
		$('.allmodels__select-text2').text($(this).text());
	});
	$('.allmodels__select3').click(function(){
		$('.allmodels__dropdown3').toggleClass('jq-selectbox__dropdown-active');
	});
	$('.allmodels__dropdown3 li').click(function(){
		$('.allmodels__dropdown3').toggleClass('jq-selectbox__dropdown-active');
		$('.allmodels__select-text3').text($(this).text());
	});
	$('.allmodels__select4').click(function(){
		$('.allmodels__dropdown4').toggleClass('jq-selectbox__dropdown-active');
	});
	$('.allmodels__dropdown4 li').click(function(){
		$('.allmodels__dropdown4').toggleClass('jq-selectbox__dropdown-active');
		$('.allmodels__select-text4').text($(this).text());
	});
	$('.allmodels__select5').click(function(){
		$('.allmodels__dropdown5').toggleClass('jq-selectbox__dropdown-active');
	});
	$('.allmodels__dropdown5 li').click(function(){
		$('.allmodels__dropdown5').toggleClass('jq-selectbox__dropdown-active');
		$('.allmodels__select-text5').text($(this).text());
	});
	
	$('.allmodels__content_right-foto').click(function(){
		$('.allmodels__content_right-foto').removeClass('allmodels__content_right-foto--active');
		$(this).addClass('allmodels__content_right-foto--active');
	});
	$('.panel__members_list-box').click(function(){
		$('.panel__members_list-box').removeClass('panel__members_list-box--active');
		$(this).addClass('panel__members_list-box--active');
	});
	$(window).keyup(function(e){
		var target = $('.checkbox-ios input:focus');
		if (e.keyCode == 9 && $(target).length){
			$(target).parent().addClass('focused');
		}
	});
	$('.checkbox-ios input').focusout(function(){
		$(this).parent().removeClass('focused');
	});
});
var button1 = document.querySelector(".aphorisms__block_setting-button1--open1");
var input1 = document.querySelector(".aphorisms__block_text-input--open1");

button1.addEventListener("click", function(){
  input1.toggleAttribute("disabled");
});

var button2 = document.querySelector(".aphorisms__block_setting-button1--open2");
var input2 = document.querySelector(".aphorisms__block_text-input--open2");

button2.addEventListener("click", function(){
  input2.toggleAttribute("disabled");
});

var button3 = document.querySelector(".aphorisms__block_setting-button1--open3");
var input3 = document.querySelector(".aphorisms__block_text-input--open3");

button3.addEventListener("click", function(){
  input3.toggleAttribute("disabled");
});

var button4 = document.querySelector(".aphorisms__block_setting-button1--open4");
var input4 = document.querySelector(".aphorisms__block_text-input--open4");

button4.addEventListener("click", function(){
  input4.toggleAttribute("disabled");
});

var button5 = document.querySelector(".information__settong_block1");
var input5 = document.querySelector(".information__settong_input");

button5.addEventListener("click", function(){
  input5.toggleAttribute("disabled");
});

var button6 = document.querySelector(".aphorisms__block_setting-button1--opendiv1");
var input61 = document.querySelector(".companies__question1");
var input62 = document.querySelector(".companies__answer1");

button6.addEventListener("click", function(){
  input61.toggleAttribute("disabled");
  input62.toggleAttribute("disabled");
});

var button7 = document.querySelector(".aphorisms__block_setting-button1--opendiv2");
var input71 = document.querySelector(".companies__question2");
var input72 = document.querySelector(".companies__answer2");

button7.addEventListener("click", function(){
  input71.toggleAttribute("disabled");
  input72.toggleAttribute("disabled");
});

var button8 = document.querySelector(".aphorisms__block_setting-button1--opendiv3");
var input81 = document.querySelector(".companies__question3");
var input82 = document.querySelector(".companies__answer3");

button8.addEventListener("click", function(){
  input81.toggleAttribute("disabled");
  input82.toggleAttribute("disabled");
});

var button9 = document.querySelector(".aphorisms__block_setting-button1--opendiv4");
var input91 = document.querySelector(".companies__question4");
var input92 = document.querySelector(".companies__answer4");

button9.addEventListener("click", function(){
  input91.toggleAttribute("disabled");
  input92.toggleAttribute("disabled");
});

var button10 = document.querySelector(".aphorisms__block_setting-button1--openbox1");
var input101 = document.querySelector(".companies__question11");
var input102 = document.querySelector(".companies__answer11");

button10.addEventListener("click", function(){
  input101.toggleAttribute("disabled");
  input102.toggleAttribute("disabled");
});

var button11 = document.querySelector(".aphorisms__block_setting-button1--openbox2");
var input111 = document.querySelector(".companies__question22");
var input112 = document.querySelector(".companies__answer22");

button11.addEventListener("click", function(){
  input111.toggleAttribute("disabled");
  input112.toggleAttribute("disabled");
});

var button12 = document.querySelector(".aphorisms__block_setting-button1--openbox3");
var input121 = document.querySelector(".companies__question33");
var input122 = document.querySelector(".companies__answer33");

button12.addEventListener("click", function(){
  input121.toggleAttribute("disabled");
  input122.toggleAttribute("disabled");
});

var button13 = document.querySelector(".aphorisms__block_setting-button1--openbox4");
var input131 = document.querySelector(".companies__question44");
var input132 = document.querySelector(".companies__answer44");

button13.addEventListener("click", function(){
  input131.toggleAttribute("disabled");
  input132.toggleAttribute("disabled");
});

var button14 = document.querySelector(".networks__block_setting-button1");
var input14 = document.querySelector(".networks__messenger_input1");

button14.addEventListener("click", function(){
  input14.toggleAttribute("disabled");
});

var button15 = document.querySelector(".networks__block_setting-button3");
var input15 = document.querySelector(".networks__messenger_input2");

button15.addEventListener("click", function(){
  input15.toggleAttribute("disabled");
});

var button16 = document.querySelector(".networks__block_setting-button5");
var input16 = document.querySelector(".networks__messenger_input3");

button16.addEventListener("click", function(){
  input16.toggleAttribute("disabled");
});

var button17 = document.querySelector(".networks__block_setting-button7");
var input17 = document.querySelector(".networks__messenger_input4");

button17.addEventListener("click", function(){
  input17.toggleAttribute("disabled");
});