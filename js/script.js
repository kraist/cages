$(document).ready(function(){
	$('.top__fixed_menu-models').click(function(){
		$('.top').addClass('top__display_filter');
		$('.inherit').addClass('main__models');
		$('.about').removeClass('main__about');
		$('.blog').removeClass('main__blog');
		$('.joinus').removeClass('main__join');
		$('body').removeClass('main__blog_scroll');
		$('body').removeClass('main__about_scroll');
		$('body').removeClass('main__join_scroll');
	});
	$('.top__fixed_menu-about').click(function(){
		$('.top').removeClass('top__display_filter');
		$('.inherit').removeClass('main__models');
		$('.about').addClass('main__about');
		$('.blog').removeClass('main__blog');
		$('.joinus').removeClass('main__join');
		$('body').removeClass('main__blog_scroll');
		$('body').addClass('main__about_scroll');
		$('body').removeClass('main__join_scroll');
	});
	$('.top__fixed_menu-blog').click(function(){
		$('.top').removeClass('top__display_filter');
		$('.inherit').removeClass('main__models');
		$('.about').removeClass('main__about');
		$('.blog').addClass('main__blog');
		$('.joinus').removeClass('main__join');
		$('body').addClass('main__blog_scroll');
		$('body').removeClass('main__about_scroll');
		$('body').removeClass('main__join_scroll');
	});
	$('.top__fixed_menu-join').click(function(){
		$('.top').removeClass('top__display_filter');
		$('.inherit').removeClass('main__models');
		$('.about').removeClass('main__about');
		$('.blog').removeClass('main__blog');
		$('.joinus').addClass('main__join');
		$('body').removeClass('main__blog_scroll');
		$('body').removeClass('main__about_scroll');
		$('body').addClass('main__join_scroll');
	});
	$('.about__contacts_mobile-phone').click(function () {
		var phone = $(this);
		phone.addClass( 'about__contacts_mobile-phone--active' ); 
		setTimeout(function(){
			phone.removeClass( 'about__contacts_mobile-phone--active' );
		}, 5000 );
	});
	$('.about__contacts_email').click(function () {
		var email = $(this);
		email.addClass( 'about__contacts_email-active' ); 
		setTimeout(function(){
			email.removeClass( 'about__contacts_email-active' );
		}, 5000 );
	});
	$(".tab__menu").click(function () {
		$('.top').addClass('top__models');
		$('.tab__view5').fadeOut();
		var index = $(this).index();
		index++;
		$('.openview'+index).fadeIn(3000);
		$(".tab__menu").removeClass("tab__menu_activ");
		$(this).addClass("tab__menu_activ");		
	});
	$('.top__fixed_filter-text').click(function(){
		$('.top__fixed_filter-flex').toggleClass('top__fixed_filter-flex--active');
	});
	$( ".bigmag" ).click(function() {
				$('.menu').toggle();
	});
	$('.polaroids__form_close-question').click(function() {
		$(this).fadeOut();
		$('.polaroids__form_close-close').fadeIn();
		$('.polaroids__form_input-name').attr('value','Serhii Kolesnyk');
		$('.polaroids__form_input').css('color','#ffffff');
		$('.polaroids__form_input').attr('disabled','disabled');
		$('.polaroids__form_input-placement').attr('value','Ukraine, Kiyv');
		$('.polaroids__form_input-nickname').attr('value','@my_insta_name');
		$('.polaroids__form_input-mail').attr('value','example@gmail.com');
		$('.polaroids__form_input-age').attr('value','22');
		$('.polaroids__form_input-tattoo').attr('value','0');
		$('.jq-selectbox__select-text4').text('Intermediate');
		$('.polaroids__select').addClass('jq-selectbox__select-text4--active');
		$('.polaroids__form_input-size').attr('value','41 EU');
		$('.polaroids__form_input-height').attr('value','189');
		$('.polaroids__form_input-chest').attr('value','95');
		$('.polaroids__form_input-waist').attr('value','65');
		$('.polaroids__form_input-hips').attr('value','90');
		$('.polaroids__form_input-eyes').attr('value','green');
		$('.polaroids__form_input-education').attr('value','secondary general education');
		$('.polaroids__form_input-hobbie').attr('value','singing, dancing');
		$('.polaroids__form_input-experience').attr('value','i have no experience');
	});
	$('.polaroids__form_close-close').click(function() {
		$('.polaroids__form_input').attr('value','');
		$(this).fadeOut();
		$('.polaroids__form_close-question').fadeIn();
		$('.polaroids__form_input').css('color','#8A8A8A');
		$('.polaroids__form_input').removeAttr('disabled','disabled');
		$('.jq-selectbox__select-text4').text('english level');
		$('.polaroids__select').removeClass('jq-selectbox__select-text4--active');
	});
	$(".inherit__models_list").click(function () {
		$('.inherit__right_block').hide();
		$('.models__tab11').css('opacity','0');
		$('.models__tab11').removeClass('models__tab131');
		var index = $(this).index();
		index++;
		$('.opendiv'+index).css('opacity','1');
		$('.opendiv'+index).addClass('models__tab131');
		$(".inherit__models_list").removeClass("inherit__models_list-activ");
		$(this).addClass("inherit__models_list-activ");		
	});
	$('.about__questions_text').click(function(){
		var $question = $(this).closest('.about__questions_block li');
		if($question.is('.quest_active')){
			$question.find('.about__questions_answer').slideUp();
		} else {
			$question.find('.about__questions_answer').slideDown();
		}
		$question.toggleClass('quest_active');
	});
	$('.about__contacts_check').click(function(){
		var $question = $(this).closest('.about__contacts li');
		if($question.is('.about__contacts_active')){
			$question.find('.about__contacts_models').slideUp();
		} else {
			$question.find('.about__contacts_models').slideDown();
		}
		$question.toggleClass('about__contacts_active');
	});
	$('.polaroids__form_button').click(function(){
		var $question = $(this).closest('.polaroids__right');
		if($question.is('.polaroids__form_active')){
			$question.find('.polaroids__form_error1').slideUp();
		} else {
			$question.find('.polaroids__form_error1').slideDown();
		}
		$question.toggleClass('polaroids__form_active');
	});
	$('.joinus__polaroids_bottom-text--name').click(function(){
		var $questions = $(this).closest('.joinus__polaroids_bottom li');
		if($questions.is('.joinus__polaroids_bottom-active')){
			$questions.find('.joinus__polaroids_bottom-text--content').slideUp();
		} else {
			$questions.find('.joinus__polaroids_bottom-text--content').slideDown();
		}
		$questions.toggleClass('joinus__polaroids_bottom-active');
	});
	$('.jq-selectbox__select1').click(function(){
		$('.jq-selectbox__dropdown1').toggleClass('jq-selectbox__dropdown1-active');
	});
	$('.jq-selectbox__dropdown1 li').click(function(){
		$('.jq-selectbox__dropdown1').toggleClass('jq-selectbox__dropdown1-active');
		$('.jq-selectbox__select-text1').text($(this).text());
	});
	$('.jq-selectbox__select2').click(function(){
		$('.jq-selectbox__dropdown2').toggleClass('jq-selectbox__dropdown2-active');
	});
	$('.jq-selectbox__dropdown2 li').click(function(){
		$('.jq-selectbox__dropdown2').toggleClass('jq-selectbox__dropdown2-active');
		$('.jq-selectbox__select-text2').text($(this).text());
	});
	$('.jq-selectbox__select3').click(function(){
		$('.jq-selectbox__dropdown3').toggleClass('jq-selectbox__dropdown3-active');
	});
	$('.jq-selectbox__dropdown3 li').click(function(){
		$('.jq-selectbox__dropdown3').toggleClass('jq-selectbox__dropdown3-active');
		$('.jq-selectbox__select-text3').text($(this).text());
	});
	$('.jq-selectbox__select4').click(function(){
		$('.jq-selectbox__dropdown4').toggleClass('jq-selectbox__dropdown4-active');
	});
	$('.jq-selectbox__dropdown4 li').click(function(){
		$('.jq-selectbox__dropdown4').toggleClass('jq-selectbox__dropdown4-active');
		$('.jq-selectbox__select-text4').text($(this).text());
	});
	$( '.about__questions_answer-bloger' ).click(function () {
		var button = $('.about__contacts_box-karta1');
		button.addClass( 'about__contacts_box-karta--active' ); 
		setTimeout(function(){
			button.removeClass( 'about__contacts_box-karta--active' );
		}, 5000 );
	});
	$(window).scroll(function() {
        if($(this).scrollTop() >= 180) {
            $('.empty').addClass('main__blog_scroll1');
			$('.top').addClass('main__blog_scroll2 animations');
        }
        else{
            $('.empty').removeClass('main__blog_scroll1');
			$('.top').removeClass('main__blog_scroll2 animations');
        }
    });
});