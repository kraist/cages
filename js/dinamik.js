function copytext(el) {
	var $tmp = $("<input>");
	$("body").append($tmp);
	$tmp.val($(el).text()).select();
	document.execCommand("copy");
	$tmp.remove();
}

let $target = $('.inherit__right_article');
let hold = 5000;
let index = 0;
setInterval(() => {
	if (index >=  $target.length)
	  index = 0;
	  
	$target.hide(); 
	$target.eq(index).show(); 
	index++;
  }, hold
);


$(document).ready(function(){
	$('.slider').slick({
		dots: false,
		arrows: true,
		centerMode: true,
		centerPadding: '50px',
		slidesToShow: 1,
		responsive: [
			{
			  breakpoint: 500,
			  settings: {
				dots: false,
				arrows: true,
				centerMode: true,
				centerPadding: '50px',
				slidesToShow: 1
			  }
			}
		]
	});
});
