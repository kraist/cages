$(document).ready(function(){	
	$(window).bind('resize', function(){
		if ($(window).width() < 981) {
			$(".model__info_article").click(function(){
				$('.model__info').toggleClass('model__info_active');
				$('.slick-arrow').toggle();
			});
			$(".tab__view1").click(function(){
				$('.main').addClass('main-models');
			});
			$(".tab__view2").click(function(){
				$('.main').removeClass('main-models');
			});
			$(".tab__view3").click(function(){
				$('.main').removeClass('main-models');
			});
			$(".tab__view4").click(function(){
				$('.main').removeClass('main-models');
			});
			$(".inherit__models_list").click(function () {
				$('.inherit__right_block').hide();
				$('.models__tab11').css('opacity','0');
				$('.models__tab11').removeClass('models__tab131');
				var index = $(this).index();
				index++;
				$(".inherit__models_list").removeClass("inherit__models_list-activ");
				$(this).addClass("inherit__models_list-activ");				
			});
			$(".inherit__models_info-check").click(function () {
				
				$('.top__models.top').css('top','-100px');
				$('.main').addClass('main-models-back');
				$('.top__models ~ .openview1 .inherit__models').css('opacity','0');
				$('.models__tab11').removeClass('models__tab132');
				var index = $('.inherit__models_list').index();
				index++;
				$('.opendiv'+index).addClass('models__tab132');
					
			});
			$(".model__info_top-back").click(function () {
				$('.top__models.top').css('top','0');
				$('.models__tab11').removeClass('models__tab131');
				$('.models__tab11').removeClass('models__tab132');
				$('.models__tab11').css('opacity','0');
				$('.top__models ~ .openview1 .inherit__models').css('opacity','1');
				$('.slick-arrow').show();
				$('.main').removeClass('main-models-back');
			});
			$(".inherit__models_info-check1").click(function () {
				$('.opendiv1').addClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check2").click(function () {
				$('.opendiv2').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check3").click(function () {
				$('.opendiv3').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check4").click(function () {
				$('.opendiv4').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check5").click(function () {
				$('.opendiv5').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check6").click(function () {
				$('.opendiv6').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check7").click(function () {
				$('.opendiv7').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check8").click(function () {
				$('.opendiv8').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check9").click(function () {
				$('.opendiv9').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
				$('.opendiv10').removeClass('models__tab132');
			});
			$(".inherit__models_info-check10").click(function () {
				$('.opendiv10').addClass('models__tab132');
				$('.opendiv1').removeClass('models__tab132');
				$('.opendiv3').removeClass('models__tab132');
				$('.opendiv4').removeClass('models__tab132');
				$('.opendiv5').removeClass('models__tab132');
				$('.opendiv6').removeClass('models__tab132');
				$('.opendiv7').removeClass('models__tab132');
				$('.opendiv8').removeClass('models__tab132');
				$('.opendiv9').removeClass('models__tab132');
				$('.opendiv2').removeClass('models__tab132');
			});
		};
		if ($(window).width() < 761) {
			$(".tab__view2").click(function(){
				$('body').addClass('about__body');
			});
			$(".tab__view1").click(function(){
				$('body').removeClass('about__body');
			});
			$(".tab__view3").click(function(){
				$('body').removeClass('about__body');
			});
			$(".tab__view4").click(function(){
				$('body').removeClass('about__body');
			});
		};
		}).trigger('resize');
});
